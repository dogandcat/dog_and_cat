﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CatFire : MonoBehaviour
{
    public RectTransform Canvas;
    public Transform LaunchAngle;

    public float Power = 10;
    public float Gravity = -10;

    public GameObject bullet;
    public Transform shotSpawn;
    public Transform childTransform;

    public Image img;

    private Vector3 MoveSpeed;
    private Vector3 GritySpeed = Vector3.zero;
    private float dTime;
    private Vector3 currentAngle;
    private int currentChildrenCount;

    private float LimitZ;

    private TurnControl turnControl;
    public GameObject Turn;

    private Timer timer;
    public GameObject Timer;

    public Transform Cam;

    public GameObject Dog;
    public GameObject Cat;
    public GameObject DogCanvas;
    public GameObject CatCanvas;

    private Animator catAnim;

    private bool isGenerate = false;

    private Vector3 bulletCurrentPosition;
    private float LimtX;
    private bool isPowerIncrease = true;
    private bool isAngleIncrease = true;

    private int clickCount = 0;
    private bool fixAngle = false;
    private bool fixPower = false;

    public AudioSource audioSource;

    void Start()
    { 
        EventTriggerListener.Get(gameObject).onDown += OnClickDown;
        turnControl = Turn.GetComponent<TurnControl>();
        timer = Timer.GetComponent<Timer>();
        //LaunchAngle.localRotation = Quaternion.Euler(0, 0, 90.0f);
        catAnim = Cat.GetComponent<Animator>();
    }
    void OnClickDown(GameObject go)
    {
        //isUp = false;
        //StartCoroutine(grow());

        if (clickCount == 0)
        {
            //角度不动 fixed angle
            fixAngle = true;
            clickCount += 1;
        }
        else if (clickCount == 1)
        {
            //能量不动 fixed power
            fixPower = true;
            //第二次点击了 second click
            clickCount += 1;
        }
        else if (clickCount == 2)
        {
            Instantiate(bullet, shotSpawn.position, shotSpawn.rotation, shotSpawn);
            InitializationBullet();
            audioSource.Play();
            //isFire = true;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //LimtX = Mathf.Clamp(LimtX, 47.0f, 128.0f);
        //先是指示标乱转  fix angle
        if (fixAngle == false)
        {
            //先选定角度，选好过后fill才动 first:fix angle, second fix power
            if (isAngleIncrease == true)
            {              
                if (LimitZ >= 39.0f)
                {
                    isAngleIncrease = false;
                }
                LimitZ += 1.0f;
            }
            else if (isAngleIncrease == false)
            {          
                if (LimitZ <= -39.0f)
                {
                    isAngleIncrease = true;
                }
                LimitZ -= 1.0f;
            }
            
            LaunchAngle.localRotation = Quaternion.Euler(0, 0, LimitZ);
        }

        if (fixAngle == true && fixPower == false)
        {
            //能量的动来动去 power keeps change 
            if (isPowerIncrease == true)
            {
                img.fillAmount += 0.03f;
                if (img.fillAmount >= 1)
                {
                    isPowerIncrease = false;
                }
            }
            else if (isPowerIncrease == false)
            {
                img.fillAmount -= 0.03f;
                if (img.fillAmount <= 0)
                {
                    isPowerIncrease = true;
                }
            }
        }

        currentChildrenCount = shotSpawn.childCount;
        if(turnControl.isCatAction == true)
        {
            Dog.GetComponent<DogControl>().enabled = false;
            Cat.GetComponent<CatControl>().enabled = true;

            DogCanvas.SetActive(false);
            if (CatCanvas.activeInHierarchy == false)
            {
                CatCanvas.SetActive(true);
            }

            if (currentChildrenCount != 0)
            {
                //catAnim.SetBool("isThrow", true);
                catAnim.SetFloat("catThrow", 2);

                //m=gt;
                //v = at ;
                bulletCurrentPosition = childTransform.position;
                GritySpeed.y = Gravity * (dTime += Time.fixedDeltaTime);

                childTransform.position += (MoveSpeed + GritySpeed) * Time.fixedDeltaTime;


                LimtX = childTransform.position.x - bulletCurrentPosition.x + Cam.position.x;
                LimtX = Mathf.Clamp(LimtX, -1.0f, 14.0f);
                Cat.GetComponent<CatControl>().enabled = false;
                Cam.position = new Vector3(LimtX, Cam.position.y, Cam.position.z);
               
                currentAngle.z = Mathf.Atan((MoveSpeed.y + GritySpeed.y) / MoveSpeed.x) * Mathf.Rad2Deg;
                childTransform.eulerAngles = currentAngle;


                isGenerate = true;           
            }

            if(currentChildrenCount==0&&isGenerate==true||(int)timer.count<0)
            {
                //the dog`s turn
                catAnim.SetFloat("catThrow", 0);
                timer.count = 20.0f;
                turnControl.isDogAction = true;
                turnControl.isCatAction = false;
                Cam.position = new Vector3(-1.0f, Cam.position.y, Cam.position.z);
                isGenerate = false;
                fixPower = false;
                fixAngle = false;
                clickCount = 0;
                img.fillAmount = 0;
                DogCanvas.SetActive(true);      
            }
        }   
    }


    void InitializationBullet()
    {
        if (shotSpawn.childCount == 1)
        {
            dTime = 0.0f;
            currentAngle = Vector3.zero;
            Power = img.fillAmount * 25.0f;

            childTransform = shotSpawn.Find("CatBullet(Clone)");
            MoveSpeed = Quaternion.Euler(0, 0, LimitZ + 40+90) * Vector3.right * Power;
        }
    }

}
