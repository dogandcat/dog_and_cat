﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CatHealth : MonoBehaviour
{
    public int catHealth;
    public Image image;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="DogBullet")
        {
            audioSource.Play();
            catHealth -= 1;
            image.fillAmount -= 0.1f;
        }
        if(catHealth == 0)
        {
            Debug.Log("Dog won this match!");
            SceneManager.LoadScene(3);
        }
    }
}
