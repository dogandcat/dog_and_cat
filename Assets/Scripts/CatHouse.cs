﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CatHouse : MonoBehaviour
{
    public Sprite house01;
    public Sprite house02;
    public Sprite house03;
    public Sprite house04;
    private int count = 5;

    public GameObject itemAttackTwice;
    public GameObject itemBrick;
    public GameObject itemInvincible;
    private GameObject item;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        audioSource.Play();
        if (collider.tag == "DogBullet")
        {
            --count;
            switch (count)
            {
                case 4:
                    gameObject.GetComponent<SpriteRenderer>().sprite = house01;
                    break;
                case 3:
                    gameObject.GetComponent<SpriteRenderer>().sprite = house02;
                    break;
                case 2:
                    gameObject.GetComponent<SpriteRenderer>().sprite = house03;
                    break;
                case 1:
                    gameObject.GetComponent<SpriteRenderer>().sprite = house04;
                    break;
                case 0:
                    Debug.Log("Dog won this match!");
                    SceneManager.LoadScene(3);
                    break;
            }
            GenerateAndLaunch();

        }
    }

    void GenerateAndLaunch()
    {
        int itemNum = Random.Range(0, 3);
        switch (itemNum)
        {
            case 0:
                item = itemAttackTwice;
                break;
            case 1:
                item = itemBrick;
                break;
            case 2:
                item = itemInvincible;
                break;
        }
        Instantiate(item, gameObject.transform.position, gameObject.transform.rotation, gameObject.transform);
    }
}
