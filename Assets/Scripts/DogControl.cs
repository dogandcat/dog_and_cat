﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogControl : MonoBehaviour
{
    public Transform Cam;

    private Vector3 CurrentPositin;
    private float Hor;
    private float LimtX;
    private float LimitMoveX;

    private Animator anim;

    public float forceMove = 40.0f;

    public AudioSource audioSource;
    private bool isPlayAudio = false;

    private void Awake()
    {
        anim = this.GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        //dog position
        CurrentPositin = transform.position;

        Hor = Input.GetAxis("Horizontal");//AD
        if (Hor > 0.05f)
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right * forceMove);
            transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        }
        else if (Hor < -0.05f)
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(-Vector2.right * forceMove);
            transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        }

        if (Mathf.Abs(Hor) > 0)
        {
            anim.SetBool("isWalk", true);
            isPlayAudio = true;
        }
        else if (Mathf.Abs(Hor) <= 0)
        {
            anim.SetBool("isWalk", false);
            isPlayAudio = false;
        }
        palyAudio();

        LimitMoveX = transform.position.x;
        LimitMoveX = Mathf.Clamp(LimitMoveX, -6.0f, 4.0f);
        transform.position = new Vector3(LimitMoveX, transform.position.y, transform.position.z);

        LimtX = Cam.position.x + transform.position.x - CurrentPositin.x;
        LimtX = Mathf.Clamp(LimtX, -1.0f, 14.0f);
        Cam.position = new Vector3(LimtX, Cam.position.y, Cam.position.z);
    }

    void palyAudio()
    {
        if (isPlayAudio == true && audioSource.isPlaying == false)
        {
            audioSource.Play();
        }
        else if (isPlayAudio == false)
        {
            audioSource.Pause();
        }
    }
}
