﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemLaunch : MonoBehaviour
{
    public Vector3 direction;

    private float Power;//24-30
    private float Angle;//-35
    private float Gravity = -30;


    private Vector3 MoveSpeed;
    private Vector3 GritySpeed = Vector3.zero;
    private float dTime;
    private Vector3 currentAngle;

    private bool isMove = true;

    void Start()
    {
        Power = Random.Range(24,31);      
        if(gameObject.transform.parent.name== "DogHouse")
        {
            direction = Vector3.right;
            Angle = 30;
        }
        else if(gameObject.transform.parent.name == "CatHouse")
        {
            direction = Vector3.left;
            Angle = -35;
        }

        MoveSpeed = Quaternion.Euler(new Vector3(0, 0, Angle)) * direction * Power;
        currentAngle = Vector3.zero;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if(isMove)
        {
            //v = at ;
            MoveSpeed = Quaternion.Euler(new Vector3(0, 0, Angle)) * direction * Power;
            currentAngle = Vector3.zero;

            GritySpeed.y = Gravity * (dTime += Time.fixedDeltaTime);

            transform.position += (MoveSpeed + GritySpeed) * Time.fixedDeltaTime;
            currentAngle.z = Mathf.Atan((MoveSpeed.y + GritySpeed.y) / MoveSpeed.x) * Mathf.Rad2Deg;
            transform.eulerAngles = currentAngle;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag=="Ground")
        {
            isMove = false;
        }
    }

}
