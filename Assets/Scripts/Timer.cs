﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float count;
    public Image timer;
    public Sprite time20;
    public Sprite time19;
    public Sprite time18;
    public Sprite time17;
    public Sprite time16;
    public Sprite time15;
    public Sprite time14;
    public Sprite time13;
    public Sprite time12;
    public Sprite time11;
    public Sprite time10;
    public Sprite time9;
    public Sprite time8;
    public Sprite time7;
    public Sprite time6;
    public Sprite time5;
    public Sprite time4;
    public Sprite time3;
    public Sprite time2;
    public Sprite time1;
    public Sprite time0;

    private void Start()
    {
        count = 20.0f;
    }

    private void Update()
    {
        count -= Time.deltaTime;
        switch((int)count)
        {
            case 20:
                timer.sprite = time20;
                break;
            case 19:
                timer.sprite = time19;
                break;
            case 18:
                timer.sprite = time18;
                break;
            case 17:
                timer.sprite = time17;
                break;
            case 16:
                timer.sprite = time16;
                break;
            case 15:
                timer.sprite = time15;
                break;
            case 14:
                timer.sprite = time14;
                break;
            case 13:
                timer.sprite = time13;
                break;
            case 12:
                timer.sprite = time12;
                break;
            case 11:
                timer.sprite = time11;
                break;
            case 10:
                timer.sprite = time10;
                break;
            case 9:
                timer.sprite = time9;
                break;
            case 8:
                timer.sprite = time8;
                break;
            case 7:
                timer.sprite = time7;
                break;
            case 6:
                timer.sprite = time6;
                break;
            case 5:
                timer.sprite = time5;
                break;
            case 4:
                timer.sprite = time4;
                break;
            case 3:
                timer.sprite = time3;
                break;
            case 2:
                timer.sprite = time2;
                break;
            case 1:
                timer.sprite = time1;
                break;
            case 0:
                timer.sprite = time0;
                break;
        }
        //timer.text = ""+(int)count;
    }
}
