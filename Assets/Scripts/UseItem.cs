﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseItem : MonoBehaviour
{
    private Transform player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void UseInvincibility()
    {
        Destroy(gameObject);
    }
    
    public void UseDoubleThrow()
    {
        Destroy(gameObject);
    }

    public void UseBrick()
    {
        Destroy(gameObject);
    }
}
